<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\Exception;

use Exception;

class MissingTransactionInRequestException extends Exception
{
}
