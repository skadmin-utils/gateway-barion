<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion;

use BarionClient;
use BarionEnvironment;
use FundingSourceType;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\InvalidLinkException;
use Nette\Utils\Validators;
use PaymentStateResponseModel;
use PreparePaymentResponseModel;
use SkadminUtils\Gateway\AGatewayFactory;
use SkadminUtils\Gateway\Transaction\AGatewayTransaction;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayRequest;
use SkadminUtils\GatewayBarion\Transaction\BarionGatewayTransaction;

class BarionFactory extends AGatewayFactory
{
    public const CODE             = 'gateway-barion';
    public const CODE_BANK        = 'gateway-barion-bank-transfer';
    public const STATUS_SUCCEEDED = 'Succeeded';

    private LinkGenerator $linkGenerator;
    private BarionClient  $barionClient;
    private string        $payerHint;
    private string        $redirectUrl;
    private string        $callbackUrl;

    public function __construct(LinkGenerator $linkGenerator, string $secretKey, int $version, bool $isTest, string $payerHint, string $redirectUrl, string $callbackUrl)
    {
        $this->linkGenerator = $linkGenerator;

        $enviroment         = $isTest ? BarionEnvironment::Test : BarionEnvironment::Prod;
        $this->barionClient = new BarionClient($secretKey, $version, $enviroment);

        $this->payerHint   = $payerHint;
        $this->redirectUrl = $redirectUrl;
        $this->callbackUrl = $callbackUrl;
    }

    /**
     * @throws Exception\MissingTransactionInRequestException
     * @throws InvalidLinkException
     */
    public function process(AGatewayTransaction|BarionGatewayTransaction $transaction, string $code): PreparePaymentResponseModel
    {
        $transaction->setPayee($this->payerHint); //@phpstan-ignore-line

        $redirectUrl = Validators::isUrl($this->redirectUrl) ? $this->redirectUrl : $this->linkGenerator->link($this->redirectUrl);
        $callbackUrl = Validators::isUrl($this->callbackUrl) ? $this->callbackUrl : $this->linkGenerator->link($this->callbackUrl);

        switch ($code) {
            case self::CODE_BANK:
                $requestFactory = new BarionGatewayRequest($transaction->getPayerEmail(), $transaction->getPayerPhoneNumber(), $redirectUrl, $callbackUrl, true, [FundingSourceType::Balance]); //@phpstan-ignore-line
                break;
            default:
                $requestFactory = new BarionGatewayRequest($transaction->getPayerEmail(), $transaction->getPayerPhoneNumber(), $redirectUrl, $callbackUrl); //@phpstan-ignore-line
        }

        $paymetRequest = $requestFactory->getGatewayRequest($transaction); //@phpstan-ignore-line

        return $this->barionClient->PreparePayment($paymetRequest);
    }

    public function getPaymentState(string $paymentId): PaymentStateResponseModel
    {
        return $this->barionClient->GetPaymentState($paymentId);
    }
}
