<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\Utils;

use Nette\Application\UI\ITemplateFactory;
use Nette\Utils\Html;
use Skadmin\Translator\Translator;

use function implode;

use const DIRECTORY_SEPARATOR;

/**
 * @Deprecatet
 * Class BarionHelper
 */
class BarionHelper
{
    private ITemplateFactory $templateFactory;
    private Translator       $translator;

    public function __construct(ITemplateFactory $ITemplateFactory, Translator $translator)
    {
        $this->templateFactory = $ITemplateFactory;
        $this->translator      = $translator;
    }

    public function renderFormFile(): ?Html //BarionOrder
    {
        die;
        // @phpstan-ignore-next-line
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormFile.latte']));
        $template->setTranslator($this->translator);

        $template->formFile = $formFile;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template);

        return $htmlTemplate;
    }
}
