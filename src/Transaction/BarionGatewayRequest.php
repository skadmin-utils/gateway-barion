<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\Transaction;

use FundingSourceType;
use PaymentType;
use PreparePaymentRequestModel;
use SkadminUtils\GatewayBarion\Exception\MissingTransactionInRequestException;

use function array_values;
use function count;
use function is_array;
use function sprintf;

class BarionGatewayRequest
{
    private string $paymentType = PaymentType::Immediate;
    private string $payerHint; // user e-mail
    private string $payerPhoneNumber; // user phone
    private string $redirectUrl;
    private string $callbackUrl;
    private bool   $guestCheckout;
    /** @var array<string> */
    private array $fundingSources;

    /**
     * @param array<string> $fundingSources
     */
    public function __construct(string $payerHint, string $payerPhoneNumber, string $redirectUrl, string $callbackUrl, bool $guestCheckout = true, array $fundingSources = [FundingSourceType::All])
    {
        $this->payerHint        = $payerHint;
        $this->payerPhoneNumber = $payerPhoneNumber;
        $this->redirectUrl      = $redirectUrl;
        $this->callbackUrl      = $callbackUrl;
        $this->guestCheckout    = $guestCheckout;
        $this->fundingSources   = $fundingSources;
    }

    /**
     * @param BarionGatewayTransaction[]|BarionGatewayTransaction|array $transactions
     */
    public function getGatewayRequest($transactions): PreparePaymentRequestModel
    {
        if (is_array($transactions)) {
            $transactions = array_values($transactions);
        } elseif ($transactions instanceof BarionGatewayTransaction) { // @phpstan-ignore-line
            $transactions = [$transactions];
        } else {
            $transactions = [];
        }

        if (count($transactions) === 0) {
            throw new MissingTransactionInRequestException('The request must have at least one transaction');
        } else {
            $transaction = $transactions[0];
        }

        $ppr = new PreparePaymentRequestModel();

        $ppr->GuestCheckout    = $this->guestCheckout;
        $ppr->PaymentType      = $this->paymentType;
        $ppr->FundingSources   = $this->fundingSources;
        $ppr->PayerHint        = $this->payerHint;
        $ppr->PayerPhoneNumber = $this->payerPhoneNumber;

        $ppr->RedirectUrl = $this->redirectUrl;
        $ppr->CallbackUrl = $this->callbackUrl;

        $ppr->PaymentRequestId = sprintf('P-%s', $transaction->getPostTansactionId());
        $ppr->OrderNumber      = $transaction->getPostTansactionId();
        $ppr->Currency         = $transaction->getCurrency();
        $ppr->Locale           = $transaction->getLocale();

        foreach ($transactions as $_transaction) {
            $ppr->AddTransaction($_transaction->getGatewayTransaction());
        }

        return $ppr;
    }
}
