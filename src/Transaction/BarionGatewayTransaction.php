<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\Transaction;

use Currency;
use PaymentTransactionModel;
use SkadminUtils\Gateway\Transaction\AGatewayTransaction;
use UILocale;

use function sprintf;
use function trim;

class BarionGatewayTransaction extends AGatewayTransaction
{
    private string $payerEmail;
    private string $payerPhoneNumber;
    private string $postTansactionId;
    /** @var BarionGatewayTransactionItem[]|array */
    private array  $items = [];
    private string $currency;
    private string $locale;
    private string $comment;
    private string $payee = '';

    /**
     * @param BarionGatewayTransactionItem[]|array $items
     */
    public function __construct(string $payerEmail, string $payerPhoneNumber, string $postTansactionId, array $items = [], string $currency = Currency::CZK, string $locale = UILocale::CZ, string $comment = '')
    {
        $this->payerEmail       = $payerEmail;
        $this->payerPhoneNumber = $payerPhoneNumber;
        $this->postTansactionId = $postTansactionId;
        $this->items            = $items;

        $this->currency = $currency;
        $this->locale   = $locale;
        $this->comment  = $comment;
    }

    public function getPayerEmail(): string
    {
        return $this->payerEmail;
    }

    public function getPayerPhoneNumber(): string
    {
        return $this->payerPhoneNumber;
    }

    public function setPayee(string $payee): void
    {
        if (trim($this->payee) !== '') {
            return;
        }

        $this->payee = $payee;
    }

    public function getPostTansactionId(): string
    {
        return $this->postTansactionId;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getGatewayTransaction(): PaymentTransactionModel
    {
        $trans = new PaymentTransactionModel();

        $trans->POSTransactionId = sprintf('T-%s', $this->postTansactionId);
        $trans->Payee            = $this->payee;
        $trans->Comment          = $this->comment;

        $total = 0;
        foreach ($this->items as $item) {
            $trans->AddItem($item->getGatewayTransactionItem());
            $total += $item->getTotalPrice();
        }

        $trans->Total = $total;

        return $trans;
    }
}
