<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\Transaction;

use ItemModel;

class BarionGatewayTransactionItem
{
    private string $name;
    private string $description;
    private int    $quantity;
    private string $unit;
    private float  $unitPrice;
    private float  $totalPrice;
    private string $sku;

    public function __construct(string $name, string $description, int $quantity, string $unit, float $unitPrice, string $sku)
    {
        $this->name        = $name;
        $this->description = $description;
        $this->quantity    = $quantity;
        $this->unit        = $unit;
        $this->unitPrice   = $unitPrice;
        $this->sku         = $sku;

        $this->totalPrice = $quantity * $unitPrice;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function getGatewayTransactionItem(): ItemModel
    {
        $item = new ItemModel();

        $item->Name        = $this->name;
        $item->Description = $this->description;
        $item->Quantity    = $this->quantity;
        $item->Unit        = $this->unit;
        $item->UnitPrice   = $this->unitPrice;
        $item->ItemTotal   = $this->totalPrice;
        $item->SKU         = $this->sku;

        return $item;
    }
}
