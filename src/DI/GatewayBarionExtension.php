<?php

declare(strict_types=1);

namespace SkadminUtils\GatewayBarion\DI;

use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use SkadminUtils\GatewayBarion\BarionFactory;

class GatewayBarionExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'secretKey'   => Expect::string()->required(),
            'version'     => Expect::int(2),
            'isTest'      => Expect::bool(true),
            'payerHint'   => Expect::email()->required(),
            'redirectUrl' => Expect::string()->required(),
            'callbackUrl' => Expect::string()->required(),
            'autowired'   => Expect::bool(true),
        ]);
    }

    public function loadConfiguration(): void
    {
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('barionFactory'))
            ->setAutowired($this->config->autowired)
            ->setClass(BarionFactory::class)
            ->setArguments([
                $builder->getDefinition('application.linkGenerator'),
                $this->config->secretKey,
                $this->config->version,
                $this->config->isTest,
                $this->config->payerHint,
                $this->config->redirectUrl,
                $this->config->callbackUrl,
            ]);
    }
}
